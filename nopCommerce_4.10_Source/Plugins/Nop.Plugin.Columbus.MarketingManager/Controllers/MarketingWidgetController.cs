﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Columbus.MarketingWidget.Domain;
using Nop.Plugin.Columbus.MarketingWidget.Models;
using Nop.Plugin.Columbus.MarketingWidget.Services;
using Nop.Web.Areas.Admin.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Columbus.MarketingWidget.Controllers
{
    public class MarketingWidgetController : BaseAdminController
    {
        private readonly IMarketingWidgetService _marketingWidgetService;

        public MarketingWidgetController(IMarketingWidgetService marketingWidgetService)
        {
            _marketingWidgetService = marketingWidgetService;
        }

        public IActionResult MarketingWidget()
        {
            var model = new MarketingWidgetModel();
            return View("~/Plugins/Columbus.MarketingWidget/Views/MarketingWidget.cshtml",model);
        }

        [HttpPost]
        public IActionResult MarketingWidget(MarketingWidgetModel model)
        {
            MarketingWidgetEntity mw = new MarketingWidgetEntity
            {
                Name = model.Name,
                IsActive = model.IsActive,
                HTMLContent = model.HTMLContent,
                Order = model.Order,
                CreatedDate = DateTime.UtcNow,
                ValidFrom = model.ValidFrom,
                ValidTo = model.ValidTo,
                WidgetLocation = model.WidgetLocation
            };

            _marketingWidgetService.Insert(mw);

            return RedirectToAction("WidgetList");
        }


        public IActionResult Edit(int Id)
        {
            var mw = _marketingWidgetService.GetWidgetbyId(Id);
            var model = new MarketingWidgetModel()
            {
                Name = mw.Name,
                IsActive = mw.IsActive,
                Order = mw.Order,
                HTMLContent = mw.HTMLContent,
                ValidFrom = mw.ValidFrom,
                ValidTo = mw.ValidTo,
                WidgetLocation = mw.WidgetLocation
            };

            return View("~/Plugins/Columbus.MarketingWidget/Views/MarketingWidget.cshtml",model);
        }

        [HttpPost]
        public IActionResult Edit(MarketingWidgetModel model)
        {
            var mw = _marketingWidgetService.GetWidgetbyId(model.Id);
            mw.Name = model.Name;
            mw.IsActive = model.IsActive;
            mw.Order = model.Order;
            mw.HTMLContent = model.HTMLContent;
            mw.ValidFrom = model.ValidFrom;
            mw.ValidTo = model.ValidTo;
            mw.WidgetLocation = model.WidgetLocation;

            _marketingWidgetService.Update(mw);
            return RedirectToAction("WidgetList");
        }

        public IActionResult Delete(int Id)
        {
            var mw = _marketingWidgetService.GetWidgetbyId(Id);
            _marketingWidgetService.Delete(mw);

            return new NullJsonResult();
        }

        public IActionResult WidgetList()
        {
            var model = new MarketingWidgetModel();
            return View("~/Plugins/Columbus.MarketingWidget/Views/WidgetList.cshtml", model);
        }

        [HttpPost]
        public IActionResult WidgetList(DataSourceRequest command, MarketingWidgetModel model)
        {
            var marketingWidgetList = _marketingWidgetService.GetWidgets(pageIndex: command.Page - 1, pageSize: command.PageSize);
            var gridModel = new DataSourceResult();

            gridModel.Data = marketingWidgetList.Select(w =>
            {
                return new MarketingWidgetModel()
                {
                    Id = w.Id,
                    Name = w.Name,
                    IsActive = w.IsActive,
                    Order = w.Order,
                    ValidFrom = w.ValidFrom,
                    ValidTo = w.ValidTo,
                    WidgetLocation = w.WidgetLocation
                };
            });

            gridModel.Total = marketingWidgetList.TotalCount;
            return Json(gridModel);
        }



    }
}
