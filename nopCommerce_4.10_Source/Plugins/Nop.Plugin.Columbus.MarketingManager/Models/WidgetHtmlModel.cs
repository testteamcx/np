﻿using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Columbus.MarketingWidget.Models
{
    public class WidgetHtmlModel : BaseNopModel
    {
        [NopResourceDisplayName("HtmlContent")]
        public string HtmlContent { get; set; }
    }
}