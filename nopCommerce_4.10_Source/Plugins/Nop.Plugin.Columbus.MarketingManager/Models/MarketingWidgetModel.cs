﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Infrastructure;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Columbus.MarketingWidget.Models
{
    public class MarketingWidgetModel : BaseNopEntityModel
    {
       
        [NopResourceDisplayName("Name")]
        public string Name { get; set; }

        public string WidgetLocation { get; set; }
        [NopResourceDisplayName("HTMLContent")]
        public string HTMLContent { get; set; }

        public int Order { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }

        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }

        public SelectList WidgetLocations {
            get
            {
                List<string> widgetZones = new List<string>
                {
                    PublicWidgetZones.HomePageTop,
                PublicWidgetZones.HomePageBottom,
                PublicWidgetZones.HomePageBeforeCategories,
                PublicWidgetZones.HomePageBeforeBestSellers,
                PublicWidgetZones.HomePageBeforeNews,
                PublicWidgetZones.HomePageBeforeProducts,

                PublicWidgetZones.CategoryDetailsTop,
                PublicWidgetZones.CategoryDetailsBottom,
                PublicWidgetZones.CategoryDetailsAfterBreadcrumb,
                PublicWidgetZones.CategoryDetailsBeforeSubcategories,
                PublicWidgetZones.CategoryDetailsAfterFeaturedProducts,
                PublicWidgetZones.CategoryDetailsBeforeFilters,
                PublicWidgetZones.CategoryDetailsBeforeProductList,

                PublicWidgetZones.ManufacturerDetailsTop,
                PublicWidgetZones.ManufacturerDetailsAfterFeaturedProducts,
                PublicWidgetZones.ManufacturerDetailsBeforeFeaturedProducts,
                PublicWidgetZones.ManufacturerDetailsBeforeFilters,
                PublicWidgetZones.ManufacturerDetailsBeforeProductList,
                PublicWidgetZones.ManufacturerDetailsBottom,

                PublicWidgetZones.ProductDetailsTop,
                PublicWidgetZones.ProductDetailsBottom,
                PublicWidgetZones.ProductDetailsAddInfo,
                PublicWidgetZones.ProductDetailsAfterBreadcrumb,
                PublicWidgetZones.ProductDetailsAfterPictures,
                PublicWidgetZones.ProductDetailsBeforeCollateral,
                PublicWidgetZones.ProductDetailsBeforePictures,
                PublicWidgetZones.ProductDetailsInsideOverviewButtonsAfter,
                PublicWidgetZones.ProductDetailsInsideOverviewButtonsBefore,
                PublicWidgetZones.ProductDetailsOverviewTop,
                PublicWidgetZones.ProductDetailsOverviewBottom
                };

                SelectList selectListItems = new SelectList(widgetZones);
                return selectListItems;
            }
        }
    }
}
