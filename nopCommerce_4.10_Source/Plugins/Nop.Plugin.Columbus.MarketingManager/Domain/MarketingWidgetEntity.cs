﻿using Nop.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Columbus.MarketingWidget.Domain
{
    public class MarketingWidgetEntity:BaseEntity
    {
        
        public string Name { get; set; }

        public string WidgetLocation { get; set; }
        public string HTMLContent { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }

        public int Order { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
    }
}
