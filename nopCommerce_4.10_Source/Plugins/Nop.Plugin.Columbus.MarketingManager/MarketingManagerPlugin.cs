﻿using Nop.Core.Plugins;
using Nop.Plugin.Columbus.MarketingWidget.Data;
using Nop.Services.Cms;
using Nop.Web.Framework.Infrastructure;
using Nop.Web.Framework.Menu;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Columbus.MarketingWidget
{
    public class MarketingWidgetPlugin : BasePlugin, IWidgetPlugin,IAdminMenuPlugin
    {
        private readonly MWObjectContext _mwObjectContext;
        public MarketingWidgetPlugin(MWObjectContext mwObjectContext)
        {
            this._mwObjectContext = mwObjectContext;
        }
        public string GetWidgetViewComponentName(string widgetZone)
        {
            return "MarketingWidgetView";
        }

        public IList<string> GetWidgetZones()
        {
            return new List<string> {
                PublicWidgetZones.HomePageTop,
                PublicWidgetZones.HomePageBottom,
                PublicWidgetZones.HomePageBeforeCategories,
                PublicWidgetZones.HomePageBeforeBestSellers,
                PublicWidgetZones.HomePageBeforeNews,
                PublicWidgetZones.HomePageBeforeProducts,

                PublicWidgetZones.CategoryDetailsTop,
                PublicWidgetZones.CategoryDetailsBottom,
                PublicWidgetZones.CategoryDetailsAfterBreadcrumb,
                PublicWidgetZones.CategoryDetailsBeforeSubcategories,
                PublicWidgetZones.CategoryDetailsAfterFeaturedProducts,
                PublicWidgetZones.CategoryDetailsBeforeFilters,
                PublicWidgetZones.CategoryDetailsBeforeProductList,

                PublicWidgetZones.ManufacturerDetailsTop,
                PublicWidgetZones.ManufacturerDetailsAfterFeaturedProducts,
                PublicWidgetZones.ManufacturerDetailsBeforeFeaturedProducts,
                PublicWidgetZones.ManufacturerDetailsBeforeFilters,
                PublicWidgetZones.ManufacturerDetailsBeforeProductList,
                PublicWidgetZones.ManufacturerDetailsBottom,

                PublicWidgetZones.ProductDetailsTop,
                PublicWidgetZones.ProductDetailsBottom,
                PublicWidgetZones.ProductDetailsAddInfo,
                PublicWidgetZones.ProductDetailsAfterBreadcrumb,
                PublicWidgetZones.ProductDetailsAfterPictures,
                PublicWidgetZones.ProductDetailsBeforeCollateral,
                PublicWidgetZones.ProductDetailsBeforePictures,
                PublicWidgetZones.ProductDetailsInsideOverviewButtonsAfter,
                PublicWidgetZones.ProductDetailsInsideOverviewButtonsBefore,
                PublicWidgetZones.ProductDetailsOverviewTop,
                PublicWidgetZones.ProductDetailsOverviewBottom

            };
        }

        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var marketingWidgetPluginNode = rootNode.ChildNodes.FirstOrDefault(w => w.SystemName == "MarketingWidget");
            if (marketingWidgetPluginNode == null)
            {
                marketingWidgetPluginNode = new SiteMapNode()
                {
                    SystemName = "MarketingWidget",
                    Title = "Marketing Widget",
                    Visible = true,
                    IconClass ="fa-gear"
                };
                rootNode.ChildNodes.Add(marketingWidgetPluginNode);
            }

            marketingWidgetPluginNode.ChildNodes.Add(new SiteMapNode()
            {
                Title = "Marketing Widget",
                Visible = true,
                IconClass = "fa-dot-circle-o",
                Url ="~/Admin/MarketingWidget/MarketingWidget"
            });

            marketingWidgetPluginNode.ChildNodes.Add(new SiteMapNode()
            {
                Title = "Widget List",
                Visible = true,
                IconClass = "fa-dot-circle-o",
                Url = "~/Admin/MarketingWidget/WidgetList"
            });
        }

        public override void Install()
        {
            _mwObjectContext.Install();
            base.Install();
        }

        public override void Uninstall()
        {
            _mwObjectContext.Uninstall();
            base.Uninstall();
        }
    }
}
