using Autofac;
using Autofac.Core;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Columbus.MarketingWidget.Data;
using Nop.Plugin.Columbus.MarketingWidget.Domain;
using Nop.Plugin.Columbus.MarketingWidget.Services;
using Nop.Services.Tax;
using Nop.Web.Framework.Infrastructure.Extensions;

namespace Nop.Plugin.Columbus.MarketingWidget.Infrastructure
{
    /// <summary>
    /// Dependency registrar
    /// </summary>
    public class DependencyRegistrar : IDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="config">Config</param>
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            builder.RegisterType<MarketingWidgetService>().As<IMarketingWidgetService>().InstancePerLifetimeScope();
            

            //data context
            builder.RegisterPluginDataContext<MWObjectContext>("nop_object_context_marketing_widget");

            //override required repository with our custom context
            builder.RegisterType<EfRepository<MarketingWidgetEntity>>().As<IRepository<MarketingWidgetEntity>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_marketing_widget"))
                .InstancePerLifetimeScope();
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order => 1;
    }
}