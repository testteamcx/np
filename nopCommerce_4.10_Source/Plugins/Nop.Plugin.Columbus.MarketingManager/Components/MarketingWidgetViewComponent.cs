﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Columbus.MarketingWidget.Models;
using Nop.Plugin.Columbus.MarketingWidget.Services;
using Nop.Web.Framework.Components;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Columbus.MarketingWidget.Components
{
    [ViewComponent(Name = "MarketingWidgetView")]
    public class MarketingWidgetViewComponent : NopViewComponent
    {
        private IMarketingWidgetService _marketingWidgetService;
        public MarketingWidgetViewComponent(IMarketingWidgetService marketingWidgetService)
        {
            this._marketingWidgetService = marketingWidgetService;
        }
        public IViewComponentResult Invoke(string widgetZone, object additionalData)
        {
            var model = new WidgetHtmlModel();
            model.HtmlContent = GetWidgetHTML(widgetZone, additionalData);
            return View("~/Plugins/Columbus.MarketingWidget/Views/WidgetHtml.cshtml",model);
        }

        private string GetWidgetHTML(string widgetZone, object additionalData)
        {
            var widgets = _marketingWidgetService.GetActiveWidgetsforLocation(widgetZone);
            string HTML = string.Empty;

            foreach (var widget in widgets)
            {
                HTML += widget.HTMLContent;
            }
            return HTML;
        }
    }
}
