﻿using Nop.Core;
using Nop.Plugin.Columbus.MarketingWidget.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Columbus.MarketingWidget.Services
{
    public interface IMarketingWidgetService
    {
        void Delete(MarketingWidgetEntity marketingWidget);
        void Insert(MarketingWidgetEntity marketingWidget);
        bool Update(MarketingWidgetEntity marketingWidget);
        IPagedList<MarketingWidgetEntity> GetWidgets(int pageIndex = 0, int pageSize = int.MaxValue);
        MarketingWidgetEntity GetWidgetbyId(int Id);

        IList<MarketingWidgetEntity> GetActiveWidgetsforLocation(string widgetLocation);
    }
}
