﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Plugin.Columbus.MarketingWidget.Domain;
using Nop.Services.Events;

namespace Nop.Plugin.Columbus.MarketingWidget.Services
{
    public class MarketingWidgetService : IMarketingWidgetService
    {
        private readonly IRepository<MarketingWidgetEntity> _widgetRepository;
        private readonly IEventPublisher _eventPublisher;        
        

        public MarketingWidgetService(IRepository<MarketingWidgetEntity> widgetRepository,
            IEventPublisher eventPublisher)
        {
            _widgetRepository = widgetRepository;
            _eventPublisher = eventPublisher;
            
        }
        public void Delete(MarketingWidgetEntity marketingWidget)
        {
            if (marketingWidget == null)
                throw new ArgumentNullException(nameof(marketingWidget));

            _widgetRepository.Delete(marketingWidget);

            //event notification
            _eventPublisher.EntityDeleted(marketingWidget);
        }

        public MarketingWidgetEntity GetWidgetbyId(int Id)
        {
            if (Id == 0)
                return null;

             return _widgetRepository.GetById(Id);
        }

        public IPagedList<MarketingWidgetEntity> GetWidgets(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = from w in _widgetRepository.Table
                        orderby w.CreatedDate descending,w.Order ascending
                        select w;
            var records = new PagedList<MarketingWidgetEntity>(query, pageIndex, pageSize);
            return records;
        }

        public void Insert(MarketingWidgetEntity marketingWidget)
        {
            
            if (marketingWidget == null)
                throw new ArgumentNullException(nameof(marketingWidget));

            _widgetRepository.Insert(marketingWidget);

            //event notification
            _eventPublisher.EntityInserted(marketingWidget);
        }

        public bool Update(MarketingWidgetEntity marketingWidget)
        {
            if (marketingWidget == null)
                throw new ArgumentNullException(nameof(marketingWidget));

            _widgetRepository.Update(marketingWidget);

            _eventPublisher.EntityUpdated(marketingWidget);
            return true;
        }

        public IList<MarketingWidgetEntity> GetActiveWidgetsforLocation(string widgetLocation)
        {
            var widgetList = (from w in _widgetRepository.Table
                        where w.IsActive && w.ValidFrom < DateTime.Now
                        && w.ValidTo > DateTime.Now &&
                              w.WidgetLocation == widgetLocation
                              orderby w.Order
                        select w).ToList();
            return widgetList;
        }
    }
}
