﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Data.Mapping;
using Nop.Plugin.Columbus.MarketingWidget.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Columbus.MarketingWidget.Data
{
    public class MarketingWidgetMap: NopEntityTypeConfiguration<MarketingWidgetEntity>
    {
        public override void Configure(EntityTypeBuilder<MarketingWidgetEntity> builder)
        {
           

            builder.ToTable(nameof(MarketingWidgetEntity));
            
            builder.Property(widget => widget.Name);
            builder.Property(widget => widget.WidgetLocation);
            builder.Property(widget => widget.HTMLContent);
            builder.Property(widget => widget.IsActive);
            builder.Property(widget => widget.Order);
            builder.Property(widget => widget.CreatedDate);
            builder.Property(widget => widget.ValidFrom);
            builder.Property(widget => widget.ValidTo);
            
        }
    }
}
