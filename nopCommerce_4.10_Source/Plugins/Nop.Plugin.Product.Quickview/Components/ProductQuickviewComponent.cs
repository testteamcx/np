﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Product.Quickview.Models;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Web.Framework.Components;
using Nop.Web.Framework.Infrastructure;
using Nop.Web.Models.Catalog;

namespace Nop.Plugin.Product.Quickview.Components
{
    [ViewComponent(Name = "ProductQuickview")]
    public class ProductQuickviewComponent : NopViewComponent
    {
        private readonly IStoreContext _storeContext;
        private readonly ISettingService _settingService;
        private readonly IProductService _productService;

        public ProductQuickviewComponent(IStoreContext storeContext, 
            ISettingService settingService,IProductService productService)
        {
            this._settingService = settingService;
            this._storeContext = storeContext;
            this._productService = productService;
        }


        public IViewComponentResult Invoke(string widgetZone, object additionalData)
        {
            if (widgetZone == PublicWidgetZones.ProductBoxAddinfoBefore)
            {
                var model = new ProductQuickviewModel();
                var prModel = ((ProductOverviewModel)additionalData);
                model.Id = prModel.Id;
                model.Sku = prModel.Sku;
                model.DefaultPictureModel = prModel.DefaultPictureModel;
                model.Name = prModel.Name;
                model.ProductPrice = prModel.ProductPrice;
                model.FullDescription = prModel.FullDescription;
                model.ReviewOverviewModel = prModel.ReviewOverviewModel;
                return View("~/Plugins/Product.Quickview/Views/Quickview.cshtml", model);
            }
            else
            {
                return View("~/Plugins/Product.Quickview/Views/sideNav.cshtml");
            }
           
        }


    }
}
