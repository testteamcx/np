﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Product.Quickview.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Plugin.Product.Quickview.Controllers
{
    [Area(AreaNames.Admin)]
    public class ProductQuickviewController : BasePluginController
    {
        private readonly IPermissionService _permissionService;
        private readonly IStoreContext _storeContext;
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;

        public ProductQuickviewController(IStoreContext storeContext, 
            IPermissionService permissionService, 
            ISettingService settingService,
            ILocalizationService localizationService
            ) {
            this._settingService = settingService;
            this._permissionService = permissionService;
            this._storeContext = storeContext;
            this._localizationService = localizationService;
        }

        #region Methods
        [AuthorizeAdmin]
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var PrQuickViewSetting = _settingService.LoadSetting<ProductQuickviewSettings>(storeScope);
            var model = new ConfigurationModel
            {
                DisplayAddToCart = PrQuickViewSetting.DisplayAddToCart
            };


            return View("~/Plugins/Product.Quickview/Views/Configure.cshtml",model);
        }


        [HttpPost]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var PrQuickViewSetting = _settingService.LoadSetting<ProductQuickviewSettings>(storeScope);

            PrQuickViewSetting.DisplayAddToCart = model.DisplayAddToCart;
            _settingService.SaveSettingOverridablePerStore(PrQuickViewSetting, x => x.DisplayAddToCart, model.DisplayAddToCart, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }
        

            #endregion
        }
}
