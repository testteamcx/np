﻿using Nop.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Product.Quickview
{
   public class ProductQuickviewSettings : ISettings
    {
        public Boolean DisplayAddToCart { get; set; }
    }
}
