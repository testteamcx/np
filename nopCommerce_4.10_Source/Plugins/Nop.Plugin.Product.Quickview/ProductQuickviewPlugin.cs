﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Plugins;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Web.Framework.Infrastructure;

namespace Nop.Plugin.Product.Quickview
{
    public class ProductQuickviewPlugin : BasePlugin, IWidgetPlugin
    {

        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly ILocalizationService _localizationService;
        public ProductQuickviewPlugin(ISettingService settingService, IWebHelper webHelper, ILocalizationService localizationService)
        {
            this._settingService = settingService;
            this._webHelper = webHelper;
            this._localizationService = localizationService;
        }

        /// <summary>
        /// Plugin Install method
        /// This method adds content to resource files
        /// Saves Default settings
        /// </summary>
        public override void Install()
        {
            base.Install();
            _localizationService.AddOrUpdatePluginLocaleResource("Nop.Plugin.Product.Quickview.Sucess", "Product Quickview settings updated successfully");
            _localizationService.AddOrUpdatePluginLocaleResource("Nop.Plugin.Product.Quickview.DisplayAddToCart", "Display Add To Cart");

        }

        /// <summary>
        ///  Plugin Uninstall Method
        /// </summary>
        public override void Uninstall()
        {
            base.Uninstall();
        }

        /// <summary>
        /// Gets a configuration page URL
        /// </summary>
        public override string GetConfigurationPageUrl()
        {
            return _webHelper.GetStoreLocation() + "Admin/ProductQuickview/Configure";
        }

        /// <summary>
        /// Gets widget zones where this widget should be rendered
        /// </summary>
        /// <returns>Widget zones</returns>
        public IList<string> GetWidgetZones()
        {
            return new List<string> { PublicWidgetZones.ProductBoxAddinfoBefore,PublicWidgetZones.BodyStartHtmlTagAfter };
        }

        /// <summary>
        ///  Gets a name of a view component for displaying widget
        /// </summary>
        /// <param name="widgetZone">Name of the widget zone</param>
        /// <returns>View component name</returns>
        public string GetWidgetViewComponentName(string widgetZone)
        {
            return "ProductQuickview";
        }
    }
}