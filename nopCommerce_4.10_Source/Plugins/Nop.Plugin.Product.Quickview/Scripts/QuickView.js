﻿function OpenQuickView(id) {
    
    //document.getElementById(id).style.width = "500px";
    
    var htmlContent = $('#' + id).html();
    $('.sidenav .content').append(htmlContent);
    $('#quickViewPanel').removeClass('hidden');

}

$(document).ready(function () {
    $('#closeBtn,.mask').click(function () {
        $('.sidenav .content').html('');
        $('#quickViewPanel').addClass('hidden');
    });
});

