﻿using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace  Nop.Plugin.Product.Quickview.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        [NopResourceDisplayName("Plugins.Widgets.ProductQuickview.DisplayAddToCart")]
        public bool DisplayAddToCart { get; set; }
        
    }
}