﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.Product.Quickview.Models
{
    public class QuickViewModel:BaseNopModel
    {
        public string Id { get; set; }

        public string SeName { get; set; }

        public string Title { get; set; }

        public string AlternateText { get; set; }

        public string ImageUrl { get; set; }

        public string Price { get; set; }

        public string AddToCartTxt { get; set; }

        public string AddToCartLink { get; set; }

        public string AddToCartBtnId { get; set; }

        public bool DisplayAddToCart { get; set; }
    }
}
