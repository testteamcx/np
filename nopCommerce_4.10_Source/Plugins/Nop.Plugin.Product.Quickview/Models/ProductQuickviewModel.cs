﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Models.Catalog;
using System.Collections.Generic;

namespace Nop.Plugin.Product.Quickview.Models
{
    public partial class ProductQuickviewModel:ProductOverviewModel
    {
        public IList<ProductPicture> pictures { get; set; }
    }
}
