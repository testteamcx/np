﻿using Microsoft.AspNetCore.Mvc.Razor;
using System.Collections.Generic;
using System.Linq;


namespace  Nop.Plugin.Product.Quickview
{
    public class ProductQuickViewLocationExpander : IViewLocationExpander
    {

        
        #region Implementation of Custom Views

        public void PopulateValues(ViewLocationExpanderContext context)
        {
                        

        }

        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {

            viewLocations = new[] {
                            $"~/Plugins/Product.Quickview/Views/Shared/{{0}}.cshtml"
                        }
                    .Concat(viewLocations);

            return viewLocations;
        }
        
        #endregion


    }
}
