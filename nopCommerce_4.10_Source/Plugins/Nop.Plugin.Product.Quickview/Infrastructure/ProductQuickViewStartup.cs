﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nop.Core.Infrastructure;

namespace Nop.Plugin.Product.Quickview
{
    public class ProductQuickViewStartup : INopStartup
    {
        
        void INopStartup.ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.ViewLocationExpanders.Add(new ProductQuickViewLocationExpander());
            });
            // Add framework services.  
            services.AddMvc();
        }
        
        public void Configure(IApplicationBuilder application)
        {


        }

        public int Order => 0;
        //#endregion
    }
}
