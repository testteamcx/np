using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.ExternalAuth.Google
{
    public static class GoogleExternalAuthConstants
    {

        public const string ProviderSystemName = "ExternalAuth.Google";
        public const string ViewComponentName = "GoogleAuthentication";
    }
}