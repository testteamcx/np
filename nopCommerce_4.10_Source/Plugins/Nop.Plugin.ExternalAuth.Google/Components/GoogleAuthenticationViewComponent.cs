using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;
using Nop.Plugin.ExternalAuth.Google;

namespace Nop.Plugin.ExternalAuth.Google.Components
{
    [ViewComponent(Name = GoogleExternalAuthConstants.ViewComponentName)]
    public class GoogleAuthenticationViewComponent : NopViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View("~/Plugins/ExternalAuth.Google/Views/PublicInfo.cshtml");
        }
    }
}