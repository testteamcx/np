﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Columbus.SEOSchema
{
    public class SEOSchemaSettings : ISettings
    {

        /// Include the settings to be used by Plugin and saved in Admin Panel
        /// 

        public bool AddSchematoHomePage { get; set; }
        public bool AddSchematoCategoryPage { get; set; }
        public bool AddSchematoManufacturerPage { get; set; }
        public bool AddSchematoProductPage { get; set; }
    }
}