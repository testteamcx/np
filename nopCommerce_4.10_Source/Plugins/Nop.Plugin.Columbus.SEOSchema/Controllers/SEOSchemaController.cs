﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Columbus.SEOSchema.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Plugin.Columbus.SEOSchema.Controllers
{
    [Area(AreaNames.Admin)]
    [AuthorizeAdmin]
    [AdminAntiForgery]
    public class SEOSchemaController : BasePluginController
    {
        #region Fields
        
        
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly IWebHelper _webHelper;
        private readonly IStoreContext _storeContext;
        #endregion

        #region Ctor

        public SEOSchemaController(
            ISettingService settingService, 
            ILocalizationService localizationService,
            IPermissionService permissionService,
            IWebHelper webHelper,
            IStoreContext storeContext)
        {
            
            this._settingService = settingService;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._webHelper = webHelper;
            this._storeContext = storeContext;
        }

        #endregion

        #region Methods
        
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var schemaSettings = _settingService.LoadSetting<SEOSchemaSettings>(storeScope);
            var model = new ConfigurationModel
            {
                AddSchematoHomePage = schemaSettings.AddSchematoHomePage,
                AddSchematoCategoryPage = schemaSettings.AddSchematoCategoryPage,
                AddSchematoManufacturerPage = schemaSettings.AddSchematoManufacturerPage,
                AddSchematoProductPage = schemaSettings.AddSchematoProductPage
            };


            
            return View("~/Plugins/Columbus.SEOSchema/Views/Configure.cshtml",model);
        }

        [HttpPost]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var schemaSetting = _settingService.LoadSetting<SEOSchemaSettings>(storeScope);

            schemaSetting.AddSchematoHomePage = model.AddSchematoHomePage;
            _settingService.SaveSettingOverridablePerStore(schemaSetting, x => x.AddSchematoHomePage, model.AddSchematoHomePage, storeScope, false);

            schemaSetting.AddSchematoCategoryPage = model.AddSchematoCategoryPage;
            _settingService.SaveSettingOverridablePerStore(schemaSetting, x => x.AddSchematoCategoryPage, model.AddSchematoCategoryPage, storeScope, false);

            schemaSetting.AddSchematoManufacturerPage = model.AddSchematoManufacturerPage;
            _settingService.SaveSettingOverridablePerStore(schemaSetting, x => x.AddSchematoManufacturerPage, model.AddSchematoManufacturerPage, storeScope, false);

            schemaSetting.AddSchematoProductPage = model.AddSchematoProductPage;
            _settingService.SaveSettingOverridablePerStore(schemaSetting, x => x.AddSchematoProductPage, model.AddSchematoProductPage, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }



        #endregion
    }
}