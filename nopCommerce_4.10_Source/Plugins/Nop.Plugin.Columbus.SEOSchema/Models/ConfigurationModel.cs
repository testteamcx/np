﻿using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace  Nop.Plugin.Columbus.SEOSchema.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        [NopResourceDisplayName("Plugins.Columbus.SEOSchema.AddSchematoHomePage")]
        public bool AddSchematoHomePage { get; set; }
        [NopResourceDisplayName("Plugins.Columbus.SEOSchema.AddSchematoCategoryPage")]
        public bool AddSchematoCategoryPage { get; set; }
        [NopResourceDisplayName("Plugins.Columbus.SEOSchema.AddSchematoManufacturerPage")]
        public bool AddSchematoManufacturerPage { get; set; }
        [NopResourceDisplayName("Plugins.Columbus.SEOSchema.AddSchematoProductPage")]
        public bool AddSchematoProductPage { get; set; }



    }
}