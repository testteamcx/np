﻿using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Columbus.SEOSchema.Models
{
    public class SchemaModel : BaseNopModel
    {
        [NopResourceDisplayName("SchemaScript")]
        public string SchemaScript { get; set; }
    }
}