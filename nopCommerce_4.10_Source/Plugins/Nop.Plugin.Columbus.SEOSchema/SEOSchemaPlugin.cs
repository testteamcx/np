using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Infrastructure;
using Nop.Core.Plugins;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Web.Framework.Infrastructure;

namespace Nop.Plugin.Columbus.SEOSchema
{
    /// <summary>
    /// PLugin
    /// </summary>
    public class SEOSchemaPlugin : BasePlugin,IWidgetPlugin
    {
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;


        public SEOSchemaPlugin(ILocalizationService localizationService,
            ISettingService settingService,
            IWebHelper webHelper
            )
        {
            this._localizationService = localizationService;
            this._settingService = settingService;
            this._webHelper = webHelper;
        }



        /// <summary>
        /// Gets a configuration page URL
        /// </summary>
        public override string GetConfigurationPageUrl()
        {
            return _webHelper.GetStoreLocation() + "Admin/SEOSchema/Configure";
        }

        public string GetWidgetViewComponentName(string widgetZone)
        {
            return "WidgetsSEOSchema";
        }

        public IList<string> GetWidgetZones()
        {
            return new List<string> { PublicWidgetZones.HomePageTop , PublicWidgetZones.CategoryDetailsTop, PublicWidgetZones.ManufacturerDetailsTop, PublicWidgetZones.ProductDetailsTop };
        }



        /// <summary>
        /// Install plugin
        /// </summary>
        public override void Install()
        {

            //settings
            var settings = new SEOSchemaSettings
            {
                AddSchematoHomePage =true,
                AddSchematoCategoryPage = true,
                AddSchematoManufacturerPage = true,
                AddSchematoProductPage = true
            };
            _settingService.SaveSetting(settings);

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Columbus.SEOSchema.AddSchematoHomePage", "Add Schema to Home Page");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Columbus.SEOSchema.AddSchematoCategoryPage", "Add Schema to Category Page");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Columbus.SEOSchema.AddSchematoManufacturerPage", "Add Schema to Manufacturer Page");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Columbus.SEOSchema.AddSchematoProductPage", "Add Schema to Product Page");

            base.Install();
        }

        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<SEOSchemaSettings>();

            //locales
            //_localizationService.DeletePluginLocaleResource("{ResourceName}");
            _localizationService.DeletePluginLocaleResource("Plugins.Columbus.SEOSchema.AddSchematoHomePage");
            _localizationService.DeletePluginLocaleResource("Plugins.Columbus.SEOSchema.AddSchematoCategoryPage");
            _localizationService.DeletePluginLocaleResource("Plugins.Columbus.SEOSchema.AddSchematoManufacturerPage");
            _localizationService.DeletePluginLocaleResource("Plugins.Columbus.SEOSchema.AddSchematoProductPage");


            base.Uninstall();
        }
    }
}