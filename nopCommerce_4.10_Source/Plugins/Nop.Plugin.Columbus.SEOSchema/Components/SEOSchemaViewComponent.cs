﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Plugin.Columbus.SEOSchema.Models;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Media;
using Nop.Web.Framework.Components;
using Nop.Web.Framework.Infrastructure;
using Nop.Web.Models.Catalog;
using Nop.Services.Seo;
namespace Nop.Plugin.Columbus.SEOSchema.Components
{
    [ViewComponent(Name = "WidgetsSEOSchema")]
    public class SEOSchemaViewComponent : NopViewComponent
    {
        
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly IProductService _productService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;

        public SEOSchemaViewComponent(IProductService productService, 
            IWebHelper webHelper,
            ISettingService settingService,
            IWorkContext workContext,
            IStoreContext storeContext)
        {
            this._productService = productService;
            this._webHelper = webHelper;
            this._settingService = settingService;
            this._workContext = workContext;
            this._storeContext = storeContext;
        }

        public IViewComponentResult Invoke(string widgetZone, object additionalData)
        {
            var schemaSettings = _settingService.LoadSetting<SEOSchemaSettings>(_storeContext.CurrentStore.Id);
            var model = new SchemaModel();
            if (widgetZone == PublicWidgetZones.HomePageTop && schemaSettings.AddSchematoHomePage)
            {
                model.SchemaScript = "<script type=\"application/ld+json\">\n" +
                                     "{\n" +
                                        "\"@context\": \"http://schema.org\",\n" +
                                        "\"@type\": \"WebSite\",\n" +
                                        "\"url\": \"" + _webHelper.GetStoreLocation(true) + "\",\n" +
                                        "\"potentialAction\": {\n" +
                                            "\"@type\": \"SearchAction\",\n" +
                                            "\"target\": \"" + _webHelper.GetStoreLocation(true) + "search?&q={query}\",\n" +
                                            "\"query-input\": \"required\"\n" +
                                        "}\n" +
                                    "}" +
                                    "</script>";
            }
            else if (widgetZone == PublicWidgetZones.CategoryDetailsTop && schemaSettings.AddSchematoCategoryPage)
            {
                var categoryModel = ((CategoryModel)additionalData);
                
                
                model.SchemaScript = "<script type=\"application/ld+json\">\n" +
                                     "{\n" +
                                        "\"@context\": \"http://schema.org\",\n" +
                                        "\"@type\": \"ItemList\",\n" +
                                        "\"url\": \"" + _webHelper.GetThisPageUrl(false) + "\",\n" +
                                        "\"numberOfItems\":\"" + categoryModel.Products.Count + "\"" +
                                        "\"itemListElement\": [" +
                                        GenerateProductListModel(categoryModel.Products) +
                                        "]\n" +
                                        "}\n" +
                                        "</script>";

            }
            else if (widgetZone == PublicWidgetZones.ManufacturerDetailsTop && schemaSettings.AddSchematoManufacturerPage)
            {
                var manufacturerModel = ((ManufacturerModel)additionalData);
                model.SchemaScript = "<script type=\"application/ld+json\">\n" +
                                   "{\n" +
                                      "\"@context\": \"http://schema.org\",\n" +
                                      "\"@type\": \"ItemList\",\n" +
                                      "\"url\": \"" + _webHelper.GetThisPageUrl(false) + "\",\n" +
                                      "\"numberOfItems\":\"" + manufacturerModel.Products.Count + "\",\n" +
                                      "\"itemListElement\": [" +
                                      GenerateProductListModel(manufacturerModel.Products) +
                                      "]\n" +
                                      "}\n" +
                                      "</script>";
            }
            else if (widgetZone == PublicWidgetZones.ProductDetailsTop && schemaSettings.AddSchematoProductPage)
            {
                var productModel = ((ProductDetailsModel)additionalData);

                model.SchemaScript = GenerateProductModel(productModel);
            }

            return View("~/Plugins/Columbus.SEOSchema/Views/SEOSchema.cshtml",model);
        }

        private string GenerateProductListModel(IList<ProductOverviewModel> products)
        {
            var categoryProductModel = string.Empty;
            for (int i = 0; i < products.Count; i++)
            {
                var productModel = products[i];
                var product = _productService.GetProductById(productModel.Id);
                categoryProductModel += "{\n" +
                                            "\"@type\": \"Product\",\n" +
                                            "\"name\":\"" + product.Name + "\",\n" +
                                            "\"image\":\"" + productModel.DefaultPictureModel.FullSizeImageUrl + "\",\n" +
                                            "\"description\":\"" + productModel.FullDescription + "\",\n" +
                                            "\"position\":\"" + (i+1) + "\",\n" +
                                            "\"url\":\"" + Url.RouteUrl("Product", new { SeName = productModel.SeName }) + "\"" +
                                            "\"offers\": {\n" +
                                                "\"@type\": \"Offer\",\n" +
                                                "\"priceCurrency\": \"" + _workContext.WorkingCurrency + "\",\n" +
                                                "\"price\": \"" + productModel.ProductPrice.PriceValue + "\",\n" +
                                                "\"availability\": \"" + (product.StockQuantity > 0 ? "https://schema.org/InStock" : "https://schema.org/OutOfStock") + "\",\n" +
                                                "\"seller\": {\n" +
                                                    "\"@type\": \"Organization\",\n" +
                                                    "\"name\": \"" + _webHelper.GetStoreLocation(true) + "\",\n" +
                                                    "\"url\": \"" + _webHelper.GetStoreLocation(true) + "\"\n" +
                                                "}\n" +
                                            (products.Count - 1 == i ? "}\n" : "},\n") +
                                            "}\n";
            }
            return categoryProductModel;
        }

        private string GenerateProductModel(ProductDetailsModel productModel)
        {
            string aggregateRating = string.Empty;
            if (productModel.ProductReviewOverview.TotalReviews > 0)
            {
                aggregateRating = productRatingSchema(productModel);

            }
            return "<script type=\"application/ld+json\">\n" +
                                        "{\n" +
                                            "\"@context\": \"http://schema.org\",\n" +
                                            "\"@type\": \"Product\",\n" +
                                            "\"name\":\"" + productModel.Name + "\",\n" +
                                            "\"image\":\"" + productModel.DefaultPictureModel.FullSizeImageUrl + "\",\n" +
                                            "\"description\":\"" + productModel.FullDescription + "\",\n" +
                                            "\"brand\":{\n" +
                                                "\"@type\":\"Thing\",\n" +
                                                "\"name\":\"" + productModel.ProductManufacturers[0].Name + "\"\n" +
                                            "},\n" +
                                            "\"offers\": {\n" +
                                                "\"@type\": \"Offer\",\n" +
                                                "\"priceCurrency\": \"" + productModel.ProductPrice.CurrencyCode + "\",\n" +
                                                "\"price\": \"" + productModel.ProductPrice.PriceValue + "\",\n" +
                                                "\"availability\": \"" + (productModel.StockAvailability == "In stock" ? "https://schema.org/InStock" : "https://schema.org/OutOfStock") + "\",\n" +
                                                "\"seller\": {\n" +
                                                    "\"@type\": \"Organization\",\n" +
                                                    "\"name\": \"" + _webHelper.GetStoreLocation(true) + "\",\n" +
                                                    "\"url\": \"" + _webHelper.GetStoreLocation(true) + "\"\n" +
                                                "}\n" +
                                            "}\n" +
                                            aggregateRating +
                                            "}" +
                                            "</script>";
            ;
                
        }

        private string productRatingSchema(ProductDetailsModel productModel)
        {
            string aggregateRating;
            var productReviews = _productService.GetAllProductReviews(customerId: 0, approved: null, fromUtc: null, toUtc: null, message: null, storeId: 0, productId: productModel.Id);
            aggregateRating = "\"aggregateRating\": {\n" +
                                    "\"@type\": \"AggregateRating\",\n" +
                                    "\"worstRating\": \"1\",\n" +
                                    "\"bestRating\": \"5\"\n" +
                                    "\"ratingValue\":\"" + productModel.ProductReviewOverview.RatingSum + "\"\n" +
                                    "\"ratingCount\":\"" + productModel.ProductReviewOverview.TotalReviews + "\"\n" +
                                    "\"reviewCount\":\"" + productModel.ProductReviewOverview.TotalReviews + "\"" +
                                "}\n";
            string reviews = "\"review\":[\n";
            for (int i = 0; i < productReviews.Count; i++)
            {
                reviews += "{\n" +
                    "\"@type\": \"Review\",\n" +
                    "\"author\": \"" + productReviews[i].Customer.SystemName + "\",\n" +
                    "\"datePublished\": \" " + productReviews[i].CreatedOnUtc + "\",\n" +
                    "\"reviewBody\": \"" + productReviews[i].ReviewText + "\",\n" +
                    "\"name\": \"" + productReviews[i].Title + "\",\n" +
                    "\"reviewRating\": {\n" +
                        "\"@type\": \"Rating\",\n" +
                        "\"ratingValue\": \"" + productReviews[i].Rating + "\"\n" +
                        "}\n" +
                     (productReviews.Count - 1 == i ? "}\n" : "},\n");
            }
            reviews += "]";

            aggregateRating += reviews;
            return aggregateRating;
        }

    }
}
