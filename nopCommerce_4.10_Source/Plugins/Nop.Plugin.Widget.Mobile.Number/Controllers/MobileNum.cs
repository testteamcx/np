﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Discounts;
using Nop.Plugin.Widget.Mobile.Number.Models;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Plugin.Widget.Mobile.Number.Controllers
{
    [AuthorizeAdmin]
    [Area(AreaNames.Admin)]
    public class MobileNum : BasePluginController
    {
        #region Fields
        private readonly MobileSettings _MobileSettings;

        private readonly ICustomerService _customerService;
        private readonly IDiscountService _discountService;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;

        #endregion

        #region Ctor

        public MobileNum(ICustomerService customerService,
            IDiscountService discountService,
            ILocalizationService localizationService,
            IPermissionService permissionService,
            ISettingService settingService)
        {
            this._customerService = customerService;
            this._discountService = discountService;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._settingService = settingService;
        }

        #endregion

       

        #region Methods
        [AuthorizeAdmin]
        public IActionResult Configure()
        {
            return View("~/Plugins/Widget.Mobile.Number/Views/Configure.cshtml");
        }


        //[HttpPost]
        //public IActionResult Configure(ConfigurationModel model)
        //{
        //    return Configure();
        //}
        #endregion        #endregion
    }
}