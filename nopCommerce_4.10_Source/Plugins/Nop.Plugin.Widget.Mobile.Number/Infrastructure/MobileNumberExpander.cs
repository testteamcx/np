﻿using Microsoft.AspNetCore.Mvc.Razor;
using System.Collections.Generic;
using System.Linq;


namespace Nop.Plugin.Widget.Mobile.Number
{
    public class MobileNumberExpander : IViewLocationExpander
    {

        
        #region Implementation of Custom Views

        public void PopulateValues(ViewLocationExpanderContext context)
        {
                        

        }

        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {


            if (context.AreaName == null && context.ViewName == "Register")
            {

                viewLocations = new string[] { $"/Plugins/Nop.Plugin.Widget.Mobile.Number/Views/Customer/{{0}}.cshtml"
                }.Concat(viewLocations);

            }
            if (context.AreaName == null && context.ViewName == "Info")
            {
                viewLocations = new string[] { $"/Plugins/Nop.Plugin.Widget.Mobile.Number/Views/Customer/{{0}}.cshtml"
                }.Concat(viewLocations);

            }

            return viewLocations;
        }
        
        #endregion


    }
}
