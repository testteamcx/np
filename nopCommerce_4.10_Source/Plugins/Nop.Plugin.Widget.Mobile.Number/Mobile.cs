using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Nop.Core.Plugins;
using Nop.Services.Configuration;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Plugin.Widget.Mobile.Number;
using Nop.Core;

namespace Nop.Plugin.Widget.Mobile.Number
{
    public partial class Mobile : BasePlugin, IMobileNumber
    {
        #region Fields

        private readonly IActionContextAccessor _actionContextAccessor;
        private readonly IDiscountService _discountService;
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly IUrlHelperFactory _urlHelperFactory;
        private readonly IWebHelper _webHelper;


        public Mobile(ISettingService settingService, IWebHelper webHelper, ILocalizationService localizationService)
        {
            this._settingService = settingService;
            this._webHelper = webHelper;
            this._localizationService = localizationService;
        }

        #endregion

        #region Ctor

        public Mobile(IActionContextAccessor actionContextAccessor,
            IDiscountService discountService,
            ILocalizationService localizationService,
            ISettingService settingService,
            IUrlHelperFactory urlHelperFactory)
        {
            this._actionContextAccessor = actionContextAccessor;
            this._discountService = discountService;
            this._localizationService = localizationService;
            this._settingService = settingService;
            this._urlHelperFactory = urlHelperFactory;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Check discount requirement
        /// </summary>
        /// <param name="request">Object that contains all information required to check the requirement (Current customer, discount, etc)</param>
        /// <returns>Result</returns>
        //public DiscountRequirementValidationResult CheckRequirement(DiscountRequirementValidationRequest request)
        //{
        //    if (request == null)
        //        throw new ArgumentNullException(nameof(request));

        //    //invalid by default
        //    var result = new DiscountRequirementValidationResult();

        //    if (request.Customer == null)
        //        return result;

        //    //try to get saved restricted customer role identifier
        //    var restrictedRoleId = _settingService.GetSettingByKey<int>(string.Format(MobileSettings.SettingsKey, request.DiscountRequirementId));
        //    if (restrictedRoleId == 0)
        //        return result;

        //    //result is valid if the customer belongs to the restricted role
        //    result.IsValid = request.Customer.CustomerRoles.Any(role => role.Id == restrictedRoleId && role.Active);

        //    return result;
        //}

        /// <summary>
        /// Get URL for rule configuration
        /// </summary>
        /// <param name="discountId">Discount identifier</param>
        /// <param name="discountRequirementId">Discount requirement identifier (if editing)</param>
        /// <returns>URL</returns>
        //public string GetConfigurationUrl(int discountId, int? discountRequirementId)
        //{
        //    var urlHelper = _urlHelperFactory.GetUrlHelper(_actionContextAccessor.ActionContext);
        //    return urlHelper.Action("Configure", "DiscountRulesCustomerRoles",
        //        new { discountId = discountId, discountRequirementId = discountRequirementId }).TrimStart('/');
        //}

        /// <summary>
        /// Install the plugin
        /// </summary>
        public override void Install()
        {
            _settingService.SaveSetting(new MobileSettings());

            //locales
            _localizationService.AddOrUpdatePluginLocaleResource("Nop.Plugin.Widget.Mobile.Number", "Mobile Number role");
            //_localizationService.AddOrUpdatePluginLocaleResource("Nop.Plugin.Widget.Mobile.Number", "Discount will be applied if customer is in the selected customer role.");
            //_localizationService.AddOrUpdatePluginLocaleResource("Nop.Plugin.Widget.Mobile.Number", "Select customer role");

            base.Install();
        }

        /// <summary>
        /// Uninstall the plugin
        /// </summary>
        public override void Uninstall()
        {
            _settingService.DeleteSetting<MobileSettings>();

            //discount requirements
            //var discountRequirements = _discountService.GetAllDiscountRequirements()
            //    .Where(discountRequirement => discountRequirement.DiscountRequirementRuleSystemName == MobileSettings.SystemName);
            //foreach (var discountRequirement in discountRequirements)
            //{
            //    _discountService.DeleteDiscountRequirement(discountRequirement);
            //}

            //locales
            _localizationService.DeletePluginLocaleResource("Nop.Plugin.Widget.Mobile.Number");
            //_localizationService.DeletePluginLocaleResource("Plugins.DiscountRules.CustomerRoles.Fields.CustomerRole.Hint");
            //_localizationService.DeletePluginLocaleResource("Plugins.DiscountRules.CustomerRoles.Fields.CustomerRole.Select");

            base.Uninstall();
        }


        public override string GetConfigurationPageUrl()
        {
            return _webHelper.GetStoreLocation() + "Admin/MobileNum/Configure";
        }
        #endregion
    }
}