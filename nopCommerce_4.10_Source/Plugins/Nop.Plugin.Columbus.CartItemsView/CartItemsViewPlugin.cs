using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Infrastructure;
using Nop.Core.Plugins;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Web.Framework.Infrastructure;

namespace CartItemsView
{
    /// <summary>
    /// PLugin
    /// </summary>
    public class CartItemsViewPlugin : BasePlugin
    {
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;

      
        public CartItemsViewPlugin(ILocalizationService localizationService,
            IPictureService pictureService,
            ISettingService settingService,
            IWebHelper webHelper,
            INopFileProvider fileProvider)
        {
            this._localizationService = localizationService;
            this._settingService = settingService;
            this._webHelper = webHelper;
        }



        /// <summary>
        /// Gets a configuration page URL
        /// </summary>

        

        /// <summary>
        /// 
        /// 
        /// Install plugin
        /// </summary>
        public override void Install()
        {

            //settings
            //var settings = new PluginSettings
           // {
           // };
            //_settingService.SaveSetting(settings);


            //_localizationService.AddOrUpdatePluginLocaleResource("{ResourceName}", "{ResourceValue}");


            base.Install();
        }

        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            //_settingService.DeleteSetting<PluginSettings>();

            //locales
            //_localizationService.DeletePluginLocaleResource("{ResourceName}");


            base.Uninstall();
        }

        

    }
}