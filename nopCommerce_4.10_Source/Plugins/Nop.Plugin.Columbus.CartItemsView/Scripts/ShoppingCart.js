﻿
$(document).ready(function () {
    //to remove item
    $('.remove-btn').click(function () {
        RemoveItem($(this));
    });


    function RemoveItem(element) {
        var result = confirm("Are you sure that you want to remove this item?");
        if (result) {
            //Logic to delete the item
            var itemId = $(element).attr('id');
            var formData = $('#shopping-cart-form').serialize();
            console.log(formData);
            $('.ajax-loading-block-window').show();
            $.ajax
                ({
                    url: '/cart/removefromcart',
                    data: { form: formData, itemId: itemId },
                    type: 'post',
                    success: function (res) {
                        console.log(res);

                        $('.ajax-loading-block-window').hide();
                        $('.page-body').html(res);
                        $('.btn_qtydecrease').on('click', function (e) {
                            e.preventDefault();
                            UpdateItemQuantity($(this), "decrease");
                        });
                        $('.btn_qtyincrease').on('click', function (e) {
                            e.preventDefault();
                            UpdateItemQuantity($(this), "increase");
                        });

                        $('.remove-btn').on('click', function (e) {
                            e.preventDefault();
                            RemoveItem($(this));
                        });



                    },
                    error: function (ex) {
                        $('.ajax-loading-block-window').hide();

                    }
                });
        }
        else
            return false;
    }
    function UpdateItemQuantity(element,action) {
        debugger;
        var itemId = $(element).attr('data-itemid');
        var qty = parseFloat(($(element).parents('.quantity').first().find("input[type='text']").val()));
        var modifiedQuantity = 0;
        if (action == "increase") {
            modifiedQuantity = qty + 1;
        }
        else {
            modifiedQuantity = qty - 1;
        }
       
       // $(element).parents('.quantity').first().find("input[type='text']").val(newQty);
        var formData = $('#shopping-cart-form').serialize();
        console.log(formData);
        $('.ajax-loading-block-window').show();
        $.ajax
            ({
                url: '/cart/Update',
                data: { form: formData, itemId: itemId, quantiy: modifiedQuantity },
                type: 'post',
                success: function (res) {
                    console.log(res);

                    $('.ajax-loading-block-window').hide();
                    $('.page-body').html(res);
                    $('.btn_qtydecrease').on('click', function (e) {
                        e.preventDefault();
                        UpdateItemQuantity($(this),"decrease");
                    });
                    $('.btn_qtyincrease').on('click', function (e) {
                        e.preventDefault();
                        UpdateItemQuantity($(this), "increase");
                    });

                    $('.remove-btn').on('click', function (e) {
                        e.preventDefault();
                        RemoveItem($(this));
                    });


                },
                error: function (ex) {
                    $('.ajax-loading-block-window').hide();

                }
            });
    }

    $(".btn_qtydecrease").on("click", function () {
    
        //Logic to delete the item
        var action = "decrease";
        UpdateItemQuantity($(this), action);

    });

    $(".btn_qtyincrease").on("click", function () {
  
        var action = "increase";
        UpdateItemQuantity($(this), action);
       

    });
});



