﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nop.Core.Infrastructure;

namespace Nop.Plugin.Columbus.CartItemsView
{
    public class CartItemsStartup : INopStartup
    {

        void INopStartup.ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<RazorViewEngineOptions>(options =>
            {
                //options.ViewLocationExpanders.Add(new viewlo());
                options.ViewLocationExpanders.Add(new CartItemsViewLocationExpander());
            });
            // Add framework services.  
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder application)
        {


        }

        public int Order => 1001;
        //#endregion


    }
}

