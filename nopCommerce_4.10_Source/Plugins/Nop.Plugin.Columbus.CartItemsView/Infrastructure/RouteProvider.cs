﻿using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework.Mvc.Routing;
using Microsoft.AspNetCore.Builder;
using Nop.Web.Framework.Localization;

namespace CartItemsView.Infrastructure
{
    public class RouteProvider : IRouteProvider
    {
        public int Priority
        {
            get
            {

                return 1;
            }
        }
        public void RegisterRoutes(IRouteBuilder routeBuilder)
        {
           



           //remove cart
            routeBuilder.MapLocalizedRoute("RemoveFromCart", "cart/removefromcart",
                new { controller = "ShoppingCart", action = "RemoveFromCart" });

            //update cart
            routeBuilder.MapLocalizedRoute("UpdateCart", "cart/update",
                new { controller = "ShoppingCart", action = "UpdateCart" });



        }
    }
}
