﻿using Microsoft.AspNetCore.Mvc.Razor;
using System.Collections.Generic;
using System.Linq;


namespace Nop.Plugin.Columbus.CartItemsView
{
    public class CartItemsViewLocationExpander : IViewLocationExpander
    {


        #region Implementation of Custom Views

        public void PopulateValues(ViewLocationExpanderContext context)
        {


        }

        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            if (context.AreaName == null && context.ViewName == "Components/OrderSummary/Default")
            {
                viewLocations = new[] {
                            $"~/Plugins/Columbus.CartItemsView/Views/Shared/Components/OrderSummary/Default.cshtml"
                        }
                    .Concat(viewLocations);
            }
            return viewLocations;
        }


        #endregion


    }
}
